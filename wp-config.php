<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cri_www_dev');

/** MySQL database username */
define('DB_USER', 'cri_www_dev');

/** MySQL database password */
define('DB_PASSWORD', 'holosync1a');

/** MySQL hostname */
define('DB_HOST', 'mysql.www-dev.centerpointe.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';Y))Ag#^h&b:AB8NkwZ#%c?CnIf!nVgwA5L5WpC/a+8@pz!v`77q8h7f2V@!?Oe+');
define('SECURE_AUTH_KEY',  'ucp7H`kjETQMBgmdg:%hX(f(%KI+RZ|#WsJ_TAaY3+"9*ZiFNRuQUpu9~snEu)ns');
define('LOGGED_IN_KEY',    'G"K35k5;+5+37w_o9vERT2CFyL;UoP"76l(d)u`JExJJ3;4/(yXCVePB0G3Xo;!$');
define('NONCE_KEY',        'ZFuoOSpqlhB&F!mDb~/*8(aAA/Z`m$D+oT9AdYS07L4IpdBs!GekoEMoTt7eSxto');
define('AUTH_SALT',        '^$l$h%y&xzdqtii6:v&!I;:YnO$5&rA?qR%mwtS%4MYOb9V(59i&IkLbx?B:Vmn_');
define('SECURE_AUTH_SALT', '?_YavBA2mKzQ*mCDc4W*AQ1fB!N*k;TK)w4hY0yp~";povj%5`L%!&BOu6BOo_bS');
define('LOGGED_IN_SALT',   'FD0It"Bbk)3kn1LP)K/bXdmN5KJ&DJpxk(K%/(aXAoD+19fKu%D;L/kd&TMT$3F9');
define('NONCE_SALT',       'p+2x|O7yzOhlD0Mp*WvLBfAI7RE/Cb:3TZA&*6u6Bx:qVK47eWsXHj""tN7`WG&8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_r2euy6_';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

