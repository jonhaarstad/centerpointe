<?php
/**
 * Single product short description
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $post;

if ( ! $post->post_excerpt ) return;
?>
<div class="small_desc" itemprop="description">
	<?php 
		if ( !isset( $data['woo_hide_small_desc'] ) || ( isset( $data['woo_hide_small_desc'] ) && $data['woo_hide_small_desc'] == 'no' )  ) {
			echo apply_filters( 'woocommerce_short_description', $post->post_excerpt );
		}
	?>	
</div>